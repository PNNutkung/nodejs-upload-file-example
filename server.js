var express = require('express');
var app = express()
var port = process.env.PORT || 3000

var morgan = require('morgan');
var multer = require('multer');
var bodyParser = require('body-parser');
var fs = require('fs');
var path = require('path');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function (req, file, cb) {
        // console.log(file.);
        var oriname = file.originalname
        var extension = oriname.substr(oriname.indexOf('.'))
        var filename = req.body.title + extension//file.originalname
        cb(null, filename) //Appending .jpg
    }
})

var uploader = multer({
    storage:storage
}).single('upl')

//require('./app/routes.js')(app,multer)

app.set('views', path.join(__dirname, 'views'))
app.set('view engine','jade')
app.use(bodyParser.json())
app.use(morgan('dev'))

app.get('/',(req,res) => {
    res.render('index')
})



app.post('/' , function(req,res){
        uploader(req,res,(err) => {
            if (err) {
                res.end('Error uploading file.')
            }
            res.end('File is uploaded.')
            console.log(req.body); //form fields
        	console.log(req.file); //form files
        	res.status(204).end();
        })
})

app.get('/delete/:id',(req,res) => {
    fs.unlink(path.join('./uploads/',req.params.id), (err) => {
        if(err) throw err
        console.log('Successfully delete!!');
    })
    res.redirect('/')
})

app.get('/uploads/:id', (req,res) => {
    //res.setHeader('Content-Type', storedMimeType)
    fs.createReadStream(path.join('./uploads/', req.params.id)).pipe(res)
})

app.listen(port, () => {
    console.log('Listening on port %d...',port);
})
